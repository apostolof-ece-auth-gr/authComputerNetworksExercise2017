import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ithakimodem.Modem;

public class ACK {
	private final static String regex = ".*PSTART.*<(.*)> ([0-9]*) PSTOP.";
	private final static String subst = "$1,$2";
	private final static Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
	
	private static final Scanner input = new Scanner(System.in);
	
	public static void ack() {
		System.out.print("Type request code for ack: ");
		String ackRequestCode = input.nextLine();
		System.out.print("Type request code for nack: ");
		String nackRequestCode = input.nextLine();
		System.out.print("Time to run: ");
		int timeToRun = input.nextInt();
		input.nextLine();
		
		getACK(ackRequestCode, nackRequestCode, timeToRun);
	}

	private static int getACK(String ackRequestCode, String nackRequestCode, int numberOfSecondsToRun) {
		long endTime = 0;
		boolean lastCallHadErrors = false;
		int packageNumber = 0, errorFreePackages = 0, errorPackages = 0, BER = 0;
		ArrayList<byte[]> packageBytesWithErrors = new ArrayList<byte[]>();
		ArrayList<Integer> packageResponseTime = new ArrayList<Integer>();
		
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("ARQ_2.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return -1;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return -1;
		}
		
		writer.println("[0] #No, [1] HasError, [2] Package, [3] Response time\n\n[0]\t[1]\t\t\t\t\t\t\t\t[2]\t\t\t\t\t\t\t\t[3]");
		
		Modem modem = new Modem();
		modem.setSpeed(Utils.modemSpeed);
		modem.setTimeout(Utils.modemTimeout);
		modem.open(Utils.modemOpen);
		
		endTime = System.currentTimeMillis() + numberOfSecondsToRun * 1000;
		while(System.currentTimeMillis() < endTime) {
			long packageStartTime = 0, packageEndTime = 0;
			String response = "";
			
			packageStartTime = System.currentTimeMillis();
			if (!lastCallHadErrors) {
				response = Utils.makeRequest(modem, ackRequestCode);
				packageEndTime = System.currentTimeMillis();
				packageResponseTime.add((int)(packageEndTime - packageStartTime - Utils.modemTimeout));
			} else {
				response = Utils.makeRequest(modem, nackRequestCode);
				packageEndTime = System.currentTimeMillis();
				packageResponseTime.add((int)(packageEndTime - packageStartTime - Utils.modemTimeout));
			}
			
			Matcher matcher = pattern.matcher(response);
			String[] packageAndFCS = (matcher.replaceAll(subst)).split(",");
			if (packageAndFCS.length != 2 || packageAndFCS[0] == null || packageAndFCS[0].isEmpty()
					|| packageAndFCS[1] == null || packageAndFCS[1].isEmpty()) {
				System.out.println("Malformed response: " + response);
				continue;
			}
			byte[] packageBytes = packageAndFCS[0].getBytes();
			byte xor = packageBytes[0];
			for (int i=1; i<packageBytes.length; ++i){
				xor = (byte) (xor^packageBytes[i]);
			}
			
			System.out.print("Package xor = " + xor + " came with xor code: " + packageAndFCS[1]);
			if (xor != Integer.parseInt(packageAndFCS[1])) {
				System.out.println(" Has errors");
				
				writer.print(packageNumber + 1 + "\t >\t");
				writer.print(packageBytes[0]);
				for (int i=1; i<packageBytes.length; ++i) {
					writer.print("," + packageBytes[i]);
				}
				if (packageBytes.toString().length() < 56) {
					writer.print("\t\t");
				} else {
					writer.print("\t");
				}
				writer.println(packageResponseTime.get(packageNumber));
				
				packageBytesWithErrors.add(packageBytes);
				++errorPackages;
				lastCallHadErrors = true;
			} else {
				System.out.println(" No errors");
				
				writer.print(packageNumber + 1 + "\t\t");
				writer.print(packageBytes[0]);
				for (int i=1; i<packageBytes.length; ++i) {
					writer.print("," + packageBytes[i]);
				}
				if (packageBytes.toString().length() < 56) {
					writer.print("\t\t");
				} else {
					writer.print("\t");
				}
				writer.println(packageResponseTime.get(packageNumber));
				
				for (byte[] errorPackage: packageBytesWithErrors) {
					for (int i=0; i<packageBytes.length; ++i) {
						int temp = errorPackage[i]^packageBytes[i];
						BER += Integer.bitCount(temp);
					}
				}
				
				packageBytesWithErrors.clear();
				++errorFreePackages;
				lastCallHadErrors = false;
			}
			++packageNumber;
		}
		
		writer.close();
		modem.close();
		
		float allPackages = errorFreePackages + errorPackages;
		System.out.println("Number of packages aquired: " + allPackages);
		System.out.println("Error free packages = " + errorFreePackages + " --> " + (errorFreePackages / allPackages * 100));
		System.out.println("Error packages = " + errorPackages + " --> " + (errorPackages / allPackages * 100));
		System.out.println("Error bits = " + BER);
		System.out.println("BER = " + ((float) BER)/((float)((allPackages) * 16 * 8)));
		
		return 1;
	}
}
