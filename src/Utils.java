import ithakimodem.Modem;

public class Utils {
	static final int modemSpeed = 80000;
	static final int modemTimeout = 1000;
	static final String modemOpen = "ithaki";
	
	static String makeRequest(Modem modem, String requestCode) {
		int responseBuffer;
		String response = "";
		
		if (!modem.write((requestCode + "\r").getBytes())) {
			return null;
		}
		
		while (true) {
			try {
				responseBuffer=modem.read();
				response += (char) responseBuffer;
				
				if (responseBuffer == -1) {
					break;
				}
			} catch (Exception x) {
				x.printStackTrace();
				return null;
			}
		}
		
		return response;
	}
}
