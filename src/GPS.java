import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ithakimodem.Modem;

public class GPS {
	private static final String extractGPGGALineRegex = ".*?(\\$.*)\\nSTOP.*";
	private static final String extractGPGGALineSubst = "$1";
	private static final Pattern extractGPGGALinePattern = Pattern.compile(extractGPGGALineRegex, Pattern.DOTALL);
	
	private static final String extractCoordinateRegex = ".+?,.+?,([0-9]+).(.+?),N,0(.+?).([0-9]+?),.+";
	private static final String extractCoordinateSubst = "$1,$2,$3,$4";
	private static final Pattern extractCoordinatePattern = Pattern.compile(extractCoordinateRegex);
	
	private static final Scanner input = new Scanner(System.in);

	public static void gps() {
		System.out.print("Type request code for the GPS : ");
		String requestCode = input.nextLine();
		System.out.print("Type any additional flags for the request: ");
		String requestFlags = input.nextLine();
		System.out.print("Enter number of coordinates to extract: ");
		int numberOfCoordinates = input.nextInt();

		List<String> coordinates = getCoordinates(requestCode, requestFlags, numberOfCoordinates);
		
		if (coordinates == null || coordinates.isEmpty()) {
			System.out.println("Came with an empty List!");
			return;
		}
		
		input.nextLine(); //Clear input buffer
		System.out.print("Should try to fetch the image? (y/n): ");
		String fetchImage = input.nextLine();
		while(true) {
			if ("y".equals(fetchImage)) {
				System.out.print("Image filename (if a file with the same name already exists"
						+ " it will be overwritten): ");
				String filename = input.nextLine();
				
				getImage(requestCode, coordinates, filename);

				return;
			} else if ("n".equals(fetchImage)) {
				return;
			}
			System.out.print("Input malformed. Try again: ");
			fetchImage = input.nextLine();
		}
	}

	private static List<String> getCoordinates(String requestCode, String requestFlag, int coordinatesToExtract) {
		List<String> coordinates, returnList = new ArrayList<String>();
		String response = "";
		
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("gps.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}

		Modem modem = new Modem();
		modem.setSpeed(Utils.modemSpeed);
		modem.setTimeout(Utils.modemTimeout);
		modem.open(Utils.modemOpen);
		
		response = Utils.makeRequest(modem, requestCode + requestFlag);
		modem.close();
		
		if (response == null || response.isEmpty()) {
			writer.close();
			return null;
		}
		
		final Matcher extractGPGGALineMatcher = extractGPGGALinePattern.matcher(response);
		coordinates = new ArrayList<String>(Arrays.asList(extractGPGGALineMatcher.replaceAll(extractGPGGALineSubst).split("\n")));
		for (String coordinate: coordinates) {
			writer.println(coordinate);
		}
		writer.close();
		writer.println(response);

		for (int i=0; i<coordinates.size(); i+=(int)((coordinates.size() - 1)/ coordinatesToExtract)) {
			String rawCoordinate = coordinates.get(i);
			final Matcher extractCoordinateMatcher = extractCoordinatePattern.matcher(rawCoordinate);
			String extractedCoordinate = extractCoordinateMatcher.replaceAll(extractCoordinateSubst);
			
			String[] coordinateSplitArray = extractedCoordinate.split(",");
			String proccessedCoordinate = coordinateSplitArray[2];
			proccessedCoordinate += (int)(Integer.parseInt(coordinateSplitArray[3].substring(0, 4)) * 0.006);
			proccessedCoordinate += coordinateSplitArray[0];
			proccessedCoordinate += (int)(Integer.parseInt(coordinateSplitArray[1]) * 0.006);
			returnList.add(proccessedCoordinate);
		}

		return returnList;
	}

	private static boolean getImage(String requestCode, List<String> coordinates, String imageFilename) {
		String imageString;
		
		for (String coordinate : coordinates) {
			requestCode += "T=" + coordinate;
		}
		
		Modem modem = new Modem();
		modem.setSpeed(Utils.modemSpeed);
		modem.setTimeout(Utils.modemTimeout);
		modem.open(Utils.modemOpen);

		{
			String response = "";
			response = Utils.makeRequest(modem, requestCode);
			if (response == null || response.isEmpty()) {
				return false;
			}
			int startIndex = response.indexOf("" + (char) 255 + (char) 216),
					endIndex = response.lastIndexOf("" + (char) 255 + (char) 217) + 2;
			if (startIndex == -1 || endIndex == -1) {
				return false;
			}
			imageString = response.substring(startIndex, endIndex);
		}
		
		FileOutputStream fileOutputStream;
		try {
			fileOutputStream = new FileOutputStream(imageFilename, false);
			
			char imageCharArray[] = imageString.toCharArray();
			byte imageByteArray[] = new byte[imageCharArray.length];
			for (int i=0; i<imageByteArray.length; ++i) {
				imageByteArray[i] = (byte)((int) imageCharArray[i]);
			}
			fileOutputStream.write(imageByteArray, 0, imageByteArray.length);
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		modem.close();
		return true;
	}
}
