import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import ithakimodem.Modem;

public class Camera {
	private static final Scanner input = new Scanner(System.in);
	
	public static void camera() {
		System.out.print("Type request code: ");
		String requestCode = input.nextLine();
		System.out.print("Image filename (if a file with the same name already exists"
				+ " it will be overwritten): ");
		String filename = input.nextLine();
		
		getImage(requestCode, filename);
	}
	
	private static boolean getImage(String requestCode, String imageFilaname) {
		String imageString;
		
		Modem modem = new Modem();
		modem.setSpeed(Utils.modemSpeed);
		modem.setTimeout(Utils.modemTimeout);
		modem.open(Utils.modemOpen);
		
		{
			String response = "";
			response = Utils.makeRequest(modem, requestCode);
			if (response == null || response.isEmpty()) {
				return false;
			}
			int startIndex = response.indexOf("" + (char) 255 + (char) 216),
					endIndex = response.lastIndexOf("" + (char) 255 + (char) 217) + 2;
			if (startIndex == -1 || endIndex == -1) {
				return false;
			}
			imageString = response.substring(startIndex, endIndex);
		}
		
		FileOutputStream fileOutputStream;
		try {
			fileOutputStream = new FileOutputStream(imageFilaname, false);
			
			char imageCharArray[] = imageString.toCharArray();
			byte imageByteArray[] = new byte[imageCharArray.length];
			for (int i=0; i<imageByteArray.length; ++i) {
				imageByteArray[i] = (byte)((int) imageCharArray[i]);
			}
			fileOutputStream.write(imageByteArray, 0, imageByteArray.length);
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		modem.close();
		return true;
	}
}
